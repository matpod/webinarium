package com.testuj.webinarium.helpers;

public enum SortingTypeEnum {
  PRICE_ASCENDING,
  NAME_ASCENDING,
  PRICE_DESCENDING,
  NAME_DESCENDING,
  NOT_SORTED
}
