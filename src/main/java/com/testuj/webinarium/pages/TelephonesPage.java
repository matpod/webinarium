package com.testuj.webinarium.pages;

import com.testuj.webinarium.configuration.Driver;
import com.testuj.webinarium.configuration.Waits;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TelephonesPage extends HeaderPage {

  @FindBy(css = "li.opened > ul > li:first-child")
  private WebElement lnkSmartphones;

  public TelephonesPage() {
    Waits.implicitlyWait(5);
    PageFactory.initElements(Driver.getInstance(), this);
  }

  public SmartphonesPage clickOnSmartphonesCategory() {
    lnkSmartphones.click();
    return new SmartphonesPage();
  }
}
