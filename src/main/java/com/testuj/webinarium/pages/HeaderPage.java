package com.testuj.webinarium.pages;

import com.testuj.webinarium.configuration.Driver;
import com.testuj.webinarium.configuration.Waits;
import org.hamcrest.core.Is;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class HeaderPage {

  @FindBy(css = "li.nav-item-2")
  private WebElement btnTelephones;

  public HeaderPage() {
    Waits.implicitlyWait(5);
    PageFactory.initElements(Driver.getInstance(), this);
  }

  public TelephonesPage clickOnTelephonesBtn() {
    Waits.awaitility(() -> assertThat(btnTelephones.isDisplayed(), is(true)));
    btnTelephones.click();
    return new TelephonesPage();
  }

}
