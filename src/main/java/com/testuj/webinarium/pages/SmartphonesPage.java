package com.testuj.webinarium.pages;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import com.google.common.collect.Ordering;
import com.testuj.webinarium.configuration.Driver;
import com.testuj.webinarium.configuration.Waits;
import com.testuj.webinarium.helpers.SortingTypeEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.stream.Collectors;

public class SmartphonesPage extends HeaderPage {

  @FindBy(css = "li[data-list-type='mini']")
  private WebElement btnListView;

  @FindBy(css = "div[data-toggle='dropdown']")
  private WebElement ddSorting;

  @FindBy(xpath = "//label[input[@value='price_asc']]")
  private WebElement btnSortByPriceAsc;

  @FindBy(xpath = "//label[input[@value='price_desc']]")
  private WebElement btnSortByPriceDesc;

  @FindBy(xpath = "//label[input[@value='name_asc']]")
  private WebElement btnSortByNameAsc;

  @FindBy(xpath = "//label[input[@value='name_desc']]")
  private WebElement btnSortByNameDesc;

  @FindBy(css = "span.price.text-nowrap")
  private List<WebElement> lstOfProductPrices;

  @FindBy(css = "a.name")
  private List<WebElement> lstOfProductsNames;

  public SmartphonesPage() {
    Waits.implicitlyWait(5);
    PageFactory.initElements(Driver.getInstance(), this);
  }

  public SmartphonesPage clickOnListView() {
    btnListView.click();
    return this;
  }

  public SmartphonesPage sortByPriceAsc() {
    ddSorting.click();
    btnSortByPriceAsc.click();
    return new SmartphonesPage();
  }

  public SmartphonesPage sortByPriceDesc() {
    ddSorting.click();
    btnSortByPriceDesc.click();
    return new SmartphonesPage();
  }

  public SmartphonesPage sortByNameAsc() {
    ddSorting.click();
    btnSortByNameAsc.click();
    return new SmartphonesPage();
  }

  public SmartphonesPage sortByNameDesc() {
    ddSorting.click();
    btnSortByNameDesc.click();
    return new SmartphonesPage();
  }

  public SmartphonesPage assertSorting(SortingTypeEnum sortingType) {
    switch (sortingType) {
      case NAME_ASCENDING:
        Waits.awaitility(() -> assertThat(Ordering.natural().isOrdered(getAllProductFirstWordsInNames()), is(true)));
        break;
      case NAME_DESCENDING:
        Waits.awaitility(() -> assertThat(Ordering.natural().reverse().isOrdered(getAllProductFirstWordsInNames()), is(true)));
        break;
      case PRICE_ASCENDING:
        Waits.awaitility(() -> assertThat(Ordering.natural().isOrdered(getAllProductPrices()), is(true)));
        break;
      case PRICE_DESCENDING:
        Waits.awaitility(() -> assertThat(Ordering.natural().reverse().isOrdered(getAllProductPrices()), is(true)));
        break;
      case NOT_SORTED:
        Waits.awaitility(() -> {
          assertThat(Ordering.natural().isOrdered(getAllProductFirstWordsInNames()), is(false));
          assertThat(Ordering.natural().reverse().isOrdered(getAllProductFirstWordsInNames()), is(false));
          assertThat(Ordering.natural().isOrdered(getAllProductPrices()), is(false));
          assertThat(Ordering.natural().reverse().isOrdered(getAllProductPrices()), is(false));
        });
        break;
    }
    return this;
  }

  public SmartphonesPage sortProducts(String sortType) {
    ddSorting.click();
    Driver.getInstance().findElement(By.xpath(String.format("//label[input[@value='%s']]", sortType))).click();
    return this;
  }

  private List<Double> getAllProductPrices() {
    Waits.awaitility(() -> lstOfProductPrices.forEach(element -> assertThat(element.isDisplayed(), is(true))));
    return lstOfProductPrices.stream()
        .map(webElement -> Double.valueOf(webElement.getText().split("zł")[0].trim().replace(",", ".").replaceAll("\\s+", "")))
        .collect(Collectors.toList());
  }

  private List<String> getAllProductFirstWordsInNames() {
    Waits.awaitility(() -> lstOfProductsNames.forEach(element -> assertThat(element.isDisplayed(), is(true))));
    return lstOfProductsNames.stream()
        .map(webElement -> webElement.getText().split("\\s+")[0])
        .collect(Collectors.toList());
  }
}
