package com.testuj.webinarium.configuration;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;

@Slf4j
public class Driver {

  private static WebDriver driver;

  public static WebDriver getInstance() {
    if (driver == null) {
      driver = setUpDriver();
    }
    return driver;
  }

  private static WebDriver setUpDriver() {
    ClassLoader classLoader = Driver.class.getClassLoader();
    File driverPath = new File(classLoader.getResource("chromedriver.exe").getFile());
    System.setProperty("webdriver.chrome.driver", String.valueOf(driverPath));

    driver = new ChromeDriver();
    driver.manage().window().maximize();
    driver.navigate().to("http://www.x-kom.pl/");
    return driver;
  }

  public static void closeDriver() {
    log.info("Closing driver");
    if (driver != null) {
      driver.manage().getCookies().clear(); //sometimes it is needed to clear cookies for safety reasons
      driver.close();
      driver.quit();
      driver = null;
    }
  }

  public static void createFailureScreenshot(String fileName) {
    log.info("Taking failure screenshot");
    File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
    try {
      FileUtils.copyFile(scrFile, new File(String.format("screenshots/%s.png", fileName)));
      log.info(String.format("Took screenshot %s.png", fileName));
    } catch (IOException e) {
      log.info("Error occurred during taking a screenshot");
      e.printStackTrace();
    }
  }

}
