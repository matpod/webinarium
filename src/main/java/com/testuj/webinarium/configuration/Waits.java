package com.testuj.webinarium.configuration;

import org.awaitility.Awaitility;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.StaleElementReferenceException;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

public class Waits {

  public static void implicitlyWait(int numberOfSeconds) {
    Driver.getInstance().manage().timeouts().implicitlyWait(numberOfSeconds, TimeUnit.SECONDS);
  }

  public static void awaitility(Runnable condition) {
    Awaitility.with().await()
        .pollInterval(1, TimeUnit.SECONDS)
        .atMost(30, TimeUnit.SECONDS)
        .ignoreException(NullPointerException.class)
        .ignoreException(NoSuchElementException.class)
        .ignoreException(ElementNotVisibleException.class)
        .ignoreException(StaleElementReferenceException.class)
        .until(condition);
  }

}
