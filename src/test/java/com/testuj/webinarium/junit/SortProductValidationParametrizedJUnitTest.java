package com.testuj.webinarium.junit;

import com.testuj.webinarium.configuration.Driver;
import com.testuj.webinarium.helpers.SortingTypeEnum;
import com.testuj.webinarium.pages.HomePage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class SortProductValidationParametrizedJUnitTest {

  private String sortingSelectorValue;
  private SortingTypeEnum sortingType;

  public SortProductValidationParametrizedJUnitTest(String sortingSelectorValue, SortingTypeEnum sortingType) {
    this.sortingSelectorValue = sortingSelectorValue;
    this.sortingType = sortingType;
  }

  @Parameters
  public static Object[][] parameters() {
    return new Object[][]{
        {"price_asc", SortingTypeEnum.PRICE_ASCENDING}, {"price_desc", SortingTypeEnum.PRICE_DESCENDING},
        {"name_asc", SortingTypeEnum.NAME_ASCENDING}, {"name_desc", SortingTypeEnum.NAME_DESCENDING},
        {"popularity_desc", SortingTypeEnum.NOT_SORTED}
    };
  }

  @Before
  public void setUp() {
    Driver.getInstance();
  }

  @Test
  public void asCustomerIWouldLikeToValidateProductsSorting() {
    //given
    HomePage homePage = new HomePage();

    //when
    homePage.clickOnTelephonesBtn()
        .clickOnSmartphonesCategory()
        .clickOnListView()
        .sortProducts(sortingSelectorValue)

        //then
        .assertSorting(sortingType);
  }

  @After
  public void cleanUp() {
    Driver.closeDriver();
  }
}
