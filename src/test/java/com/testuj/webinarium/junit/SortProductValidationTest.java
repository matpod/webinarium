package com.testuj.webinarium.junit;

import com.testuj.webinarium.configuration.Driver;
import com.testuj.webinarium.helpers.SortingTypeEnum;
import com.testuj.webinarium.pages.HomePage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SortProductValidationTest {

  private HomePage homePage;

  @Before
  public void setUp() {
    Driver.getInstance();
  }

  @Test
  public void asCustomerIShallSortProductsByPriceAscending() {

    //given
    homePage = new HomePage();

    //when
    homePage.clickOnTelephonesBtn()
        .clickOnSmartphonesCategory()
        .clickOnListView()
        .sortByPriceAsc()

        //then
        .assertSorting(SortingTypeEnum.PRICE_ASCENDING);
  }

  @Test
  public void asCustomerIShallSortProductsByPriceDescending() {

    //given
    homePage = new HomePage();

    //when
    homePage.clickOnTelephonesBtn()
        .clickOnSmartphonesCategory()
        .clickOnListView()
        .sortByPriceDesc()

        //then
        .assertSorting(SortingTypeEnum.PRICE_DESCENDING);
  }

  @Test
  public void asCustomerIShallSortProductsByNameAscending() {

    //given
    homePage = new HomePage();

    //when
    homePage.clickOnTelephonesBtn()
        .clickOnSmartphonesCategory()
        .clickOnListView()
        .sortByNameAsc()

        //then
        .assertSorting(SortingTypeEnum.NAME_ASCENDING);
  }

  @Test
  public void asCustomerIShallSortProductsByNameDescending() {

    //given
    homePage = new HomePage();

    //when
    homePage.clickOnTelephonesBtn()
        .clickOnSmartphonesCategory()
        .clickOnListView()
        .sortByNameDesc()

        //then
        .assertSorting(SortingTypeEnum.NAME_DESCENDING);
  }

  @Test
  public void asCustomerIShallSeeThatByDefaultProductsAreSortedByPopularity() {

    //given
    homePage = new HomePage();

    //when
    homePage.clickOnTelephonesBtn()
        .clickOnSmartphonesCategory()
        .clickOnListView()

        //then
        .assertSorting(SortingTypeEnum.NOT_SORTED);
  }

  @After
  public void cleanUp() {
    Driver.closeDriver();
  }

}
