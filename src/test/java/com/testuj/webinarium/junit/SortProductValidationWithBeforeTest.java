package com.testuj.webinarium.junit;

import com.testuj.webinarium.helpers.SortingTypeEnum;
import com.testuj.webinarium.listeners.junit.CustomJUnitRunner;
import com.testuj.webinarium.pages.HomePage;
import com.testuj.webinarium.pages.SmartphonesPage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(CustomJUnitRunner.class)
public class SortProductValidationWithBeforeTest {

  private SmartphonesPage smartphonesPage;

  @Before
  public void setUp() {
    HomePage homePage = new HomePage();

    smartphonesPage = homePage.clickOnTelephonesBtn()
        .clickOnSmartphonesCategory()
        .clickOnListView();
  }

  @Test
  public void asCustomerIShallSortProductsByPriceAscending() {

    //given & when
    smartphonesPage
        .sortByPriceAsc()

        //then
        .assertSorting(SortingTypeEnum.PRICE_ASCENDING);
  }

  @Test
  public void asCustomerIShallSortProductsByPriceDescending() {

    //given & when
    smartphonesPage
        .sortByPriceDesc()

        //then
        .assertSorting(SortingTypeEnum.PRICE_DESCENDING);
  }

  @Test
  public void asCustomerIShallSortProductsByNameAscending() {

    //given & when
    smartphonesPage
        .sortByNameAsc()

        //then
        .assertSorting(SortingTypeEnum.NAME_ASCENDING);
  }

  @Test
  public void asCustomerIShallSortProductsByNameDescending() {

    //given & when
    smartphonesPage
        .sortByNameDesc()

        //then
        .assertSorting(SortingTypeEnum.NAME_DESCENDING);
  }

  @Test
  public void asCustomerIShallSeeThatByDefaultProductsAreSortedByPopularity() {

    //given & when
    smartphonesPage

        //then
        .assertSorting(SortingTypeEnum.NOT_SORTED);
  }

}
