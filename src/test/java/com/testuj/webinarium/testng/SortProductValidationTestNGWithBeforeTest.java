package com.testuj.webinarium.testng;

import com.testuj.webinarium.configuration.Driver;
import com.testuj.webinarium.helpers.SortingTypeEnum;
import com.testuj.webinarium.listeners.testng.CustomTestNGListener;
import com.testuj.webinarium.pages.HomePage;
import com.testuj.webinarium.pages.SmartphonesPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(CustomTestNGListener.class)
public class SortProductValidationTestNGWithBeforeTest {

  private SmartphonesPage smartphonesPage;

  @BeforeMethod
  public void setUp() {
    Driver.getInstance();
    HomePage homePage = new HomePage();

    smartphonesPage = homePage.clickOnTelephonesBtn()
        .clickOnSmartphonesCategory()
        .clickOnListView();
  }

  @Test
  public void asCustomerIShallSortProductsByPriceAscending() {

    //given & when
    smartphonesPage
        .sortByPriceAsc()

        //then
        .assertSorting(SortingTypeEnum.PRICE_ASCENDING);
  }

  @Test
  public void asCustomerIShallSortProductsByPriceDescending() {

    //given & when
    smartphonesPage
        .sortByPriceDesc()

        //then
        .assertSorting(SortingTypeEnum.PRICE_DESCENDING);
  }

  @Test
  public void asCustomerIShallSortProductsByNameAscending() {

    //given & when
    smartphonesPage
        .sortByNameAsc()

        //then
        .assertSorting(SortingTypeEnum.NAME_ASCENDING);
  }

  @Test
  public void asCustomerIShallSortProductsByNameDescending() {

    //given & when
    smartphonesPage
        .sortByNameDesc()

        //then
        .assertSorting(SortingTypeEnum.NAME_DESCENDING);
  }

  @Test
  public void asCustomerIShallSeeThatByDefaultProductsAreSortedByPopularity() {

    //given & when
    smartphonesPage

        //then
        .assertSorting(SortingTypeEnum.NOT_SORTED);
  }

  @AfterMethod
  public void cleanUp() {
    Driver.closeDriver();
  }

}
