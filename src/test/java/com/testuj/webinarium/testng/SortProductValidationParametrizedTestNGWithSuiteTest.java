package com.testuj.webinarium.testng;

import com.testuj.webinarium.configuration.Driver;
import com.testuj.webinarium.helpers.SortingTypeEnum;
import com.testuj.webinarium.pages.HomePage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class SortProductValidationParametrizedTestNGWithSuiteTest {

  @BeforeMethod
  public void setUp() {
    Driver.getInstance();
  }

  @Test
  @Parameters({"sortingSelectorValue", "sortingType"})
  public void asCustomerIWouldLikeToValidateProductsSorting(String sortingSelectorValue, SortingTypeEnum sortingType) {
    //given
    HomePage homePage = new HomePage();

    //when
    homePage.clickOnTelephonesBtn()
        .clickOnSmartphonesCategory()
        .clickOnListView()
        .sortProducts(sortingSelectorValue)

        //then
        .assertSorting(sortingType);
  }

  @AfterMethod
  public void cleanUp() {
    Driver.closeDriver();
  }
}
