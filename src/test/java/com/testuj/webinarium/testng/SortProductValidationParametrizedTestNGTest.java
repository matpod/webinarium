package com.testuj.webinarium.testng;

import com.testuj.webinarium.configuration.Driver;
import com.testuj.webinarium.helpers.SortingTypeEnum;
import com.testuj.webinarium.listeners.testng.CustomTestNGListener;
import com.testuj.webinarium.pages.HomePage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(CustomTestNGListener.class)
public class SortProductValidationParametrizedTestNGTest {

  @DataProvider(name = "asCustomerIWouldLikeToValidateProductsSorting")
  public static Object[][] parameters() {
    return new Object[][]{
        {"price_asc", SortingTypeEnum.PRICE_ASCENDING}, {"price_desc", SortingTypeEnum.PRICE_DESCENDING},
        {"name_asc", SortingTypeEnum.NAME_ASCENDING}, {"name_desc", SortingTypeEnum.NAME_DESCENDING},
        {"popularity_desc", SortingTypeEnum.NOT_SORTED}
    };
  }

  @BeforeMethod
  public void setUp() {
    Driver.getInstance();
  }

  @Test(dataProvider = "asCustomerIWouldLikeToValidateProductsSorting")
  public void asCustomerIWouldLikeToValidateProductsSorting(String sortingSelectorValue, SortingTypeEnum sortingType) {
    //given
    HomePage homePage = new HomePage();

    //when
    homePage.clickOnTelephonesBtn()
        .clickOnSmartphonesCategory()
        .clickOnListView()
        .sortProducts(sortingSelectorValue)

        //then
        .assertSorting(sortingType);
  }

  @AfterMethod
  public void cleanUp() {
    Driver.closeDriver();
  }
}
