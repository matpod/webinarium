package com.testuj.webinarium.listeners.testng;

import com.testuj.webinarium.configuration.Driver;
import lombok.extern.slf4j.Slf4j;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

@Slf4j
public class CustomTestNGListener implements ITestListener, ISuiteListener, IInvokedMethodListener {

  @Override
  public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
    log.info(String.format("Before method invocation, during test %s", iTestResult.getName()));
  }

  @Override
  public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
    log.info(String.format("After method invocation, during test %s", iTestResult.getName()));
  }

  @Override
  public void onStart(ISuite iSuite) {
    log.info("Started executing test suite: " + iSuite.getName());
  }

  @Override
  public void onFinish(ISuite iSuite) {
    log.info("Finished executing test suite: " + iSuite.getName());
  }

  @Override
  public void onTestStart(ITestResult iTestResult) {
    log.info("Started executing test: " + iTestResult.getName());
  }

  @Override
  public void onTestSuccess(ITestResult iTestResult) {
    log.info("Test " + iTestResult.getName() + " was successful");
  }

  @Override
  public void onTestFailure(ITestResult iTestResult) {
    Driver.createFailureScreenshot(iTestResult.getName() +  + System.currentTimeMillis());
    log.info("Test failed " + iTestResult.getName());
    Driver.closeDriver();
  }

  @Override
  public void onTestSkipped(ITestResult iTestResult) {
    log.info("Test skipped: " + iTestResult.getName());
  }

  @Override
  public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
    log.info("Test failed but marked with annotation success percentage: " + iTestResult.getName());
  }

  @Override
  public void onStart(ITestContext iTestContext) {
    log.info("On start of test class: " + iTestContext.getName());
  }

  @Override
  public void onFinish(ITestContext iTestContext) {
    log.info("On end of test class: " + iTestContext.getName());
  }
}
