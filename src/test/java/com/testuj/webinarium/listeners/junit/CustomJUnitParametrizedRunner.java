package com.testuj.webinarium.listeners.junit;

import org.junit.runner.notification.RunNotifier;
import org.junit.runners.Parameterized;

public class CustomJUnitParametrizedRunner extends Parameterized {

  /**
   * Only called reflectively. Do not use programmatically.
   *
   * @param klass
   */
  public CustomJUnitParametrizedRunner(Class<?> klass) throws Throwable {
    super(klass);
  }

  @Override
  public void run(RunNotifier notifier) {
    notifier.addListener(new CustomJUnitListener());
    notifier.fireTestRunStarted(getDescription());
    super.run(notifier);
  }
}
