package com.testuj.webinarium.listeners.junit;

import com.testuj.webinarium.configuration.Driver;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;

@Slf4j
public class CustomJUnitListener extends RunListener {

  @Override
  public void testRunStarted(Description description) throws Exception {
    log.info("Started executing test run: " + description.getClassName());
  }

  @Override
  public void testRunFinished(Result result) throws Exception {
    log.info(String.format("Finished executing test run.\n%d tests failed out of %d total.", result.getFailureCount(), result.getRunCount()));
  }

  @Override
  public void testStarted(Description description) throws Exception {
    log.info("Started executing test: " + description.getMethodName());
    Driver.getInstance();
  }

  @Override
  public void testFinished(Description description) throws Exception {
    log.info("Finished executing test: " + description.getMethodName());
    Driver.closeDriver();
  }

  @Override
  public void testFailure(Failure failure) throws Exception {
    Driver.createFailureScreenshot(failure.getDescription().getMethodName() + System.currentTimeMillis());
    log.info("Test failed. " + failure.getMessage());
    Driver.closeDriver();
  }

  @Override
  public void testAssumptionFailure(Failure failure) {
    log.info("Test assumption failed. " + failure.getMessage());
  }

  @Override
  public void testIgnored(Description description) throws Exception {
    log.info("Test ignored: " + description.getMethodName());
  }

}
