package com.testuj.webinarium.listeners.junit;

import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.InitializationError;

public class CustomJUnitRunner extends BlockJUnit4ClassRunner {
  /**
   * Creates a BlockJUnit4ClassRunner to run {@code klass}
   *
   * @param klass
   * @throws InitializationError if the test class is malformed.
   */
  public CustomJUnitRunner(Class<?> klass) throws InitializationError {
    super(klass);
  }

  @Override
  public void run(RunNotifier notifier) {
    notifier.addListener(new CustomJUnitListener());
    notifier.fireTestRunStarted(getDescription());
    super.run(notifier);
  }
}
